//  
//  PlayingField.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using FireIsLib.Providers;

namespace FireIsLib
{
	public struct PlayingFieldCell
	{
		public Tetrimino Tetrimino { get; set; }
		
		/// <summary>
		/// Gets or sets a value indicating whether this instance is active.
		/// Inactive cells should be considered as empty cells.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is active; otherwise, <c>false</c>.
		/// </value>
		public bool IsActive { get; set; }
	}
	
	public class PlayingField
	{
		private const int _tetriminoDefaultXPositon = 0;
		private const int _playingFieldHeightDefault = 20;
		private const int _playingFieldWidthDefault = 10;
		private ITetriminoProvider _tetrimono;
		private PlayingFieldCell[,] _playingFieldCells;
		private Random _random;
		private Tetrimino _fallingTetrimino;
		
		public PlayingField (ITetriminoProvider tetrimino)
		{
			_tetrimono = tetrimino;
			_random = new Random ();
			ResetPlayingField ();
		}

		public int GetPlayingFieldHeight { get { return _playingFieldHeightDefault; } }

		public int GetPlayingFieldWidth { get { return _playingFieldWidthDefault; } }
		
		public PlayingFieldCell[,] GetPlayingFieldCells ()
		{
			return _playingFieldCells;
		}
		
		public void ResetPlayingField ()
		{
			_playingFieldCells = new PlayingFieldCell[GetPlayingFieldHeight, GetPlayingFieldWidth];
			_fallingTetrimino = GetRandomTetrimino ();
			PutTetrimino (_fallingTetrimino);
		}
		
		public void PutTetrimino (Tetrimino tetrimino, bool setActive = true)
		{
			for (int i = tetrimino.CellPosX; i < tetrimino.CellPosX + tetrimino.GetShape(tetrimino.CurrentShape).GetLength(0); i++) {
				if (i >= GetPlayingFieldHeight || i < 0) {
					continue;
				}
				for (int j = tetrimino.CellPosY; j < tetrimino.CellPosY + tetrimino.GetShape(tetrimino.CurrentShape).GetLength(1); j++) {	
					if (j >= GetPlayingFieldWidth || j < 0) {
						continue;
					}
					if (_fallingTetrimino.GetShape (tetrimino.CurrentShape) [i - _fallingTetrimino.CellPosX, j - _fallingTetrimino.CellPosY]) {
						_playingFieldCells [i, j].IsActive = setActive;
						_playingFieldCells [i, j].Tetrimino = tetrimino;
					}
				}
			}
		}
		
		public bool DryPutTetrimino (Tetrimino tetrimino)
		{
			for (int i = tetrimino.CellPosX; i < tetrimino.CellPosX + tetrimino.GetShape(tetrimino.CurrentShape).GetLength(0); i++) {
				for (int j = tetrimino.CellPosY; j < tetrimino.CellPosY + tetrimino.GetShape(tetrimino.CurrentShape).GetLength(1); j++) {	
					if (_fallingTetrimino.GetShape (tetrimino.CurrentShape) [i - _fallingTetrimino.CellPosX, j - _fallingTetrimino.CellPosY]) {
						if (i >= GetPlayingFieldHeight || i < 0) {
							return false;
						} else if (j >= GetPlayingFieldWidth || j < 0) {
							return false;
						} else if (_playingFieldCells [i, j].IsActive) {
							return false;
						}
					}
				}
			}
			return true;
		}
		
		public void RotateTetrimino90DegreeRight ()
		{
			PutTetrimino (_fallingTetrimino, false);
			_fallingTetrimino.RotateTetrimino90DegreeRight ();
			if (!DryPutTetrimino(_fallingTetrimino)) {
				_fallingTetrimino.RotateTetrimino90DegreeLeft();
			}
			PutTetrimino (_fallingTetrimino, true);
		}

		public void RotateTetrimino90DegreeLeft ()
		{
			PutTetrimino (_fallingTetrimino, false);
			_fallingTetrimino.RotateTetrimino90DegreeLeft ();
			if (!DryPutTetrimino(_fallingTetrimino)) {
				_fallingTetrimino.RotateTetrimino90DegreeRight();
			}
			PutTetrimino (_fallingTetrimino, true);
		}
		
		/// <summary>
		/// Moves the tetrimino down one cell.
		/// Renders new falling tetrimino if current tetrimino can't move further.
		/// </summary>
		/// <returns>
		/// Returns false if tetrimino can not be moved down and put to its current position.
		/// This indicates playingfield is full and game is over.
		/// </returns>
		public bool MoveTetriminoDown ()
		{
			PutTetrimino (_fallingTetrimino, false);
			_fallingTetrimino.CellPosX += 1;
			if (DryPutTetrimino(_fallingTetrimino)) {
				PutTetrimino (_fallingTetrimino, true);
			} else {
				_fallingTetrimino.CellPosX -= 1;
				PutTetrimino (_fallingTetrimino, true);
				_fallingTetrimino = GetRandomTetrimino();
				if (DryPutTetrimino(_fallingTetrimino)) {
					PutTetrimino (_fallingTetrimino, true);
				} else {
					return false; // Game over.
				}
			}
			return true;
		}
		
		/// <summary>
		/// Moves the tetrimino to bottom of playingfield.
		/// </summary>
		/// <returns>
		/// Returns true or false depending on MoveTetriminoDown() result.
		/// False indicates plaingfield is full and game is over.
		/// </returns>
		public bool MoveTetriminoToBottom() {
			var moveStatus = false;
			var currentTetrimino = _fallingTetrimino;
			// Drops out of loop as soon falling tetrimino
			// no longer is the same, or MoveTetrimino indicate game is over.
			do {
				moveStatus = MoveTetriminoDown();
			} while(moveStatus && _fallingTetrimino == currentTetrimino);
			return moveStatus;
		}
		
		
		public int TryToRemoveSolidRows() {
			var numberOfRowsRemoved = 0;
			if (_fallingTetrimino.CellPosX != _tetriminoDefaultXPositon) {
				return numberOfRowsRemoved;
			}
			bool isSolidRow;
			for (int i=0; i < GetPlayingFieldHeight; i++) {
				isSolidRow = true;
				for (int j = 0; j < GetPlayingFieldWidth; j++) {
					if (!GetPlayingFieldCells()[i,j].IsActive) {
						isSolidRow = false;
						break;
					}
				}
				if (isSolidRow) {
					ClearRow(i);
					numberOfRowsRemoved++;
				}
			}
			return numberOfRowsRemoved;
		}
		
		private void ClearRow(int rowIndex) {
			PutTetrimino (_fallingTetrimino, false);
			var newPlayingFieldCells = new PlayingFieldCell[GetPlayingFieldHeight, GetPlayingFieldWidth];
			for (int i=0; i < GetPlayingFieldHeight; i++) {
				for (int j = 0; j < GetPlayingFieldWidth; j++) 
				{
					if (rowIndex == i) {
						break;
					} else if (rowIndex > i && i + 1 < GetPlayingFieldHeight && _playingFieldCells[i,j].IsActive) {
						newPlayingFieldCells[i+1,j] = _playingFieldCells[i,j];
					} else {
						newPlayingFieldCells[i,j] = _playingFieldCells[i,j];
					}
				}
			}
			_playingFieldCells = newPlayingFieldCells;
			PutTetrimino (_fallingTetrimino, true);
		}
		
		public void MoveTetriminoRight ()
		{
			PutTetrimino (_fallingTetrimino, false);
			_fallingTetrimino.CellPosY += 1;
			if (!DryPutTetrimino(_fallingTetrimino)) {
				_fallingTetrimino.CellPosY -= 1;
			}
			PutTetrimino (_fallingTetrimino, true);
		}
		
		public void MoveTetriminoLeft ()
		{
			PutTetrimino (_fallingTetrimino, false);
			_fallingTetrimino.CellPosY -= 1;
			if (!DryPutTetrimino(_fallingTetrimino)) {
				_fallingTetrimino.CellPosY += 1;
			}
			PutTetrimino (_fallingTetrimino, true);
		}
		
		public Tetrimino GetRandomTetrimino ()
		{
			_tetrimono.Reset();
			var variants = _tetrimono.GetTetriminoVariants ();
			var index = _random.Next (0, variants.Length - 1);
			var newTetrimino = variants[index];
			newTetrimino.CellPosX = _tetriminoDefaultXPositon;
			newTetrimino.CellPosY = _playingFieldWidthDefault / 2 - newTetrimino.GetShape ().GetLength (1) / 2 - _random.Next (2);
			return newTetrimino;
		}
		
		public bool CellContainTheFallingTetrimino(PlayingFieldCell cell) {
			return cell.IsActive && cell.Tetrimino == _fallingTetrimino;
		}
	}
}


