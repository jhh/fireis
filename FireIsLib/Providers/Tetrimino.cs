//  
//  Tetrimino.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections;

namespace FireIsLib.Providers
{
	public class Tetrimino : IComparable<Tetrimino>
	{

		public string Name{ get; set; }
		
		public enum Shapes
		{
			Shape0=0,
			Shape90,
			Shape180,
			Shape270
		}
		
		public bool[,] GetShape (Shapes shape = Shapes.Shape0)
		{
			switch (shape) {
			case (Shapes.Shape90):
				CurrentShape = Shapes.Shape90;
				return Shape90;
			case (Shapes.Shape180):
				CurrentShape = Shapes.Shape180;
				return Shape180;
			case (Shapes.Shape270):
				CurrentShape = Shapes.Shape270;
				return Shape270;
			default:
				CurrentShape = Shapes.Shape0;
				return Shape0;
			}
		}
		
		public Shapes CurrentShape { get; internal set; }
		
		public bool[,] Shape0 { protected get; set; }

		public bool[,] Shape90 { protected get; set; }
		
		public bool[,] Shape180 { protected get; set; }
		
		public bool[,] Shape270 { protected get; set; }
		
		public int CellPosX { get; set; }
		
		public int CellPosY { get; set; }
		
		public Shapes RotateTetrimino90DegreeRight ()
		{
			switch (CurrentShape) {
			case(Tetrimino.Shapes.Shape0):
				CurrentShape = Tetrimino.Shapes.Shape90;
				break;
			case(Tetrimino.Shapes.Shape90):
				CurrentShape = Tetrimino.Shapes.Shape180;
				break;
			case(Tetrimino.Shapes.Shape180):
				CurrentShape = Tetrimino.Shapes.Shape270;
				break;
			case(Tetrimino.Shapes.Shape270):
				CurrentShape = Tetrimino.Shapes.Shape0;
				break;
			}
			return CurrentShape;
		}
		
		public Shapes RotateTetrimino90DegreeLeft ()
		{
			switch (CurrentShape) {
			case(Tetrimino.Shapes.Shape0):
				CurrentShape = Tetrimino.Shapes.Shape270;
				break;
			case(Tetrimino.Shapes.Shape90):
				CurrentShape = Tetrimino.Shapes.Shape0;
				break;
			case(Tetrimino.Shapes.Shape180):
				CurrentShape = Tetrimino.Shapes.Shape90;
				break;
			case(Tetrimino.Shapes.Shape270):
				CurrentShape = Tetrimino.Shapes.Shape180;
				break;
			}
			return CurrentShape;
		}
		
		
		/// <summary>
		/// Compares this Shape0 with other.Shape0.
		/// </summary>
		/// <returns>
		/// 0 if same.
		/// +1 if this is larger by StructuralComparer rules, otherwise -1.
		/// If array size is different: +1 if this is larger than other, otherwise -1.
		/// </returns>
		/// <param name='other'>
		/// Other, Tetrimino object.
		/// </param>
		public int CompareTo (Tetrimino other)
		{
			var compareShapesResult = 0;
			if (Shape0 != null && other.Shape0 == null) {
				compareShapesResult += 1;
			} else if (Shape0 == null && other.Shape0 != null) {
				compareShapesResult += -1;
			} else if (Shape0 != null && 
			    other.Shape0 != null) {
				compareShapesResult += CompareTwo2DArrays (Shape0, other.Shape0);
			}
			return compareShapesResult;
		}
		
		internal static bool[] Convert2DArrayInto1D (bool[,] array2d)
		{
			var al1d = new bool[array2d.GetLength (0) * array2d.GetLength (1)];
			var position = 0;
			for (int i = 0; i < array2d.GetLength(0); i++) {
				for (int j = 0; j < array2d.GetLength(1); j++) {
					al1d [position++] = array2d [i, j];
				}
			}
			return al1d;
		}
		
		internal static int CompareTwo2DArrays (bool[,] a, bool[,] b)
		{
			var a1d = Convert2DArrayInto1D (a);
			var b1d = Convert2DArrayInto1D (b);
			if (a1d.Length > b1d.Length) {
				return 1;
			} else if (a1d.Length < b1d.Length) {
				return -1;
			}
			return StructuralComparisons.StructuralComparer.Compare (a1d, b1d);
		}
	}
}

