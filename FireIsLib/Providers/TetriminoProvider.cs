//  
//  TetriminoProvider.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;

namespace FireIsLib.Providers
{
	public class TetriminoProvider : ITetriminoProvider
	{
		
		private Tetrimino[] _tetriminoVariants;
		
		public TetriminoProvider ()
		{
			Reset();
		}
		
		private Tetrimino[] GetTetriminos ()
		{
			return new Tetrimino[]
			{
				new Tetrimino{
					Name="I",
					Shape0=new bool[4, 3]{{false, true, false},{false, true, false},{false, true, false},{false, true, false}},
					Shape90 = new bool[3, 4]{{false, false, false, false},{false, false, false, false}, {true,true,true,true}}, 
					Shape180 = new bool[4, 3]{{false, true, false},{false, true, false},{false, true, false},{false, true, false}},
					Shape270 = new bool[3, 4]{{false, false, false, false},{false, false, false, false}, {true,true,true,true}}
				},
				new Tetrimino{
					Name="J",
					Shape0 = new bool[3, 3]{{false, true, false},{false, true, false}, {true, true, false}},
					Shape90 = new bool[2, 3]{{true, false, false},{true, true, true}},
					Shape180 = new bool[3,3]{{false,true, true}, {false, true, false}, {false, true, false}},
					Shape270 = new bool[3, 3]{{false, false, false},{true, true, true},{false, false, true}}
				},
				new Tetrimino{
					Name="L",
					Shape0=new bool[3, 3]{{false, true, false},{false, true, false},{false, true, true}},
					Shape90=new bool[3, 3]{{false, false, false},{true, true, true},{true, false, false}},
					Shape180=new bool[3, 3]{{true, true, false},{false, true, false},{false, true, false}},
					Shape270=new bool[2, 3]{{false, false, true},{true, true, true}}
				},
				new Tetrimino{
					Name="O",
					Shape0=new bool[2, 2]{{true,true},{true,true}},
					Shape90=new bool[2, 2]{{true,true},{true,true}},
					Shape180=new bool[2, 2]{{true,true},{true,true}},
					Shape270=new bool[2, 2]{{true,true},{true,true}}
				},
				new Tetrimino{
					Name="S",
					Shape0=new bool[2, 3]{{false, true, true},{true,true,false}},
					Shape90=new bool[3, 2]{{true, false},{true,true},{false, true}},
					Shape180=new bool[2, 3]{{false, true, true},{true,true,false}},
					Shape270=new bool[3, 2]{{true, false},{true,true},{false, true}}
				},
				new Tetrimino{
					Name="T",
					Shape0=new bool[3, 3]{{false,false,false},{true,true,true},{false,true,false}},
					Shape90=new bool[3, 3]{{false,true, false},{true, true, false},{false,true, false}},
					Shape180=new bool[3, 3]{{false,true,false},{true,true,true}, {false,false,false}},
					Shape270=new bool[3, 3]{{false,true, false},{false,true,true},{false,true,false}}
				},
				new Tetrimino{
					Name="Z",
					Shape0=new bool[2, 3]{{true,true,false},{false,true,true}},
					Shape90=new bool[3, 2]{{false, true},{true,true},{true, false}},
					Shape180=new bool[2, 3]{{true,true,false},{false,true,true}},
					Shape270=new bool[3, 2]{{false, true},{true,true},{true, false}}
				}
			};
		}
		
		public Tetrimino[]  GetTetriminoVariants ()
		{
			return _tetriminoVariants;
		}
		
		public void Reset() {
			_tetriminoVariants = GetTetriminos ();
		}
		
		public Tetrimino GetTetriminoByName (string name)
		{
			foreach (Tetrimino t in GetTetriminoVariants()) {
				if (t.Name.Equals (name)) {
					return t;
				}
			}
			throw new ArgumentException (string.Format ("There is no Tetrimino correspond to \"{0}\"", name));
		}
	}
}

