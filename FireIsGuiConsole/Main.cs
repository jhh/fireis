//  
//  Main.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using FireIsLib;
using FireIsLib.Providers;

namespace FireIsGuiConsole
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var provider = new TetriminoProvider ();
			var game = new PlayingField (provider);
			var frameAnimation = false;
			var frameTimeout = 500;
			var gameOver = false;
			var spacebarKeyAlreadyPressed = false;
			var linesRemoved = 0;
			ConsoleKeyInfo cki;
			DateTime timeoutSinceLastDown = DateTime.Now.AddMilliseconds (frameTimeout);
			do {
				var gameCells = game.GetPlayingFieldCells ();
				frameAnimation = !frameAnimation;
				for (int i = 0; i < game.GetPlayingFieldHeight; i++) {
					for (int j = 0; j < game.GetPlayingFieldWidth; j++) {
						if (game.CellContainTheFallingTetrimino (gameCells [i, j])) {
							Console.Write ("O");
						} else if (gameCells [i, j].IsActive) {
							Console.Write ("#");
						} else {
							Console.Write ("_");
						}
					}
					Console.WriteLine ();
				}
				
				Console.WriteLine ("Lines removed: " + linesRemoved + ". ");
				
				if (gameOver) {
					Console.Write ("Game over! ");
				} 
				
				Console.WriteLine ("Press ESC to quit, R to reset. " + (frameAnimation ? "/" : "\\"));
				
				cki = ConsoleHelper.ReadKeyWithTimeOut(frameTimeout);
				if (cki.Key == ConsoleKey.R) {
					game.ResetPlayingField ();
					gameOver = false;
				} else if (!gameOver) {
					switch (cki.Key) {
					case(ConsoleKey.X):
						game.RotateTetrimino90DegreeRight ();
						break;
					case(ConsoleKey.Z):
						game.RotateTetrimino90DegreeLeft ();
						break;
					case(ConsoleKey.UpArrow):
						game.RotateTetrimino90DegreeLeft ();
						break;
					case(ConsoleKey.DownArrow):
						gameOver = !game.MoveTetriminoDown ();
						timeoutSinceLastDown = DateTime.Now.AddMilliseconds (frameTimeout);
						break;
					case(ConsoleKey.RightArrow):
						game.MoveTetriminoRight ();
						break;
					case(ConsoleKey.LeftArrow):
						game.MoveTetriminoLeft ();
						break;
					case(ConsoleKey.Spacebar):
						if (!spacebarKeyAlreadyPressed) {
							gameOver = !game.MoveTetriminoToBottom ();
							spacebarKeyAlreadyPressed = !spacebarKeyAlreadyPressed;
							timeoutSinceLastDown = DateTime.Now.AddMilliseconds (frameTimeout);
						}
						break;
					}
					if (DateTime.Now >= timeoutSinceLastDown) {
						game.MoveTetriminoDown();
						timeoutSinceLastDown = DateTime.Now.AddMilliseconds (frameTimeout);
					}
				}
				if (cki.Key != ConsoleKey.Spacebar) {
					spacebarKeyAlreadyPressed = false;
				}
				
				if (!gameOver) {
					linesRemoved += game.TryToRemoveSolidRows ();
				}
				
				Console.Clear ();
				
			} while(cki.Key != ConsoleKey.Escape);
		}
	}
	
	/// <summary>
	/// Console helper.
	/// Credit to Rudolf @ http://hen.co.za/blog/2011/07/console-readkey-with-timeout/
	/// </summary>
	public static class ConsoleHelper
	{
		public static ConsoleKeyInfo ReadKeyWithTimeOut (int timeOutMS)
		{
			DateTime timeoutvalue = DateTime.Now.AddMilliseconds (timeOutMS);
			while (DateTime.Now < timeoutvalue) {
				if (Console.KeyAvailable) {
					ConsoleKeyInfo cki = Console.ReadKey ();
					return cki;
				} else {
					System.Threading.Thread.Sleep (100);
				}
			}
			return new ConsoleKeyInfo('\n', ConsoleKey.DownArrow, false, false, false);
		}
	}
}
