//  
//  PlayingFieldTest.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using NUnit.Framework;
using FireIsLib;
using FireIsLib.Providers;

namespace FireIsLibTest
{
	[TestFixture()]
	public class PlayingFieldTest
	{
		private PlayingField _pf;
		
		[SetUp]
		public void Initialize ()
		{
			_pf = new PlayingField (new TetriminoProvider ());
		}
		
		[Test]
		public void GetPlayingFieldHeightTest ()
		{
			int expected = 20;
			Assert.AreEqual (expected, _pf.GetPlayingFieldHeight, string.Format ("PlayingField's height is {0}", expected));
		}
		
		[Test]
		public void GetPlayingFieldWidthTest ()
		{
			int expected = 10;
			Assert.AreEqual (expected, _pf.GetPlayingFieldWidth, string.Format ("PlayingField's width is {0}", expected));
		}
		
		[Test]
		public void NewPlayingFieldDoNoteConsistsOfOnlyInactiveCellsTest ()
		{
			bool allInactive = true;
			foreach (var pc in _pf.GetPlayingFieldCells()) {
				if (pc.IsActive) {
					allInactive = false;
					break;
				}
			}
			Assert.IsFalse (allInactive);
		}

		[Test]
		public void NewPlayingFieldHaveOneAndExactlyOneActiveTetriminoTest ()
		{
			bool exactlyOne = true;
			Tetrimino tetr = null;
			foreach (var pc in _pf.GetPlayingFieldCells()) {
				if (pc.IsActive) {
					if (tetr == null) {
						tetr = pc.Tetrimino;
					} else if (tetr != pc.Tetrimino) {
						exactlyOne = false;
					}
				}
			}
			Assert.IsTrue (exactlyOne);
		}
		
		[Test]
		public void GetRandomTetriminoReturnsATetriminoTest ()
		{
			Assert.IsTrue (_pf.GetRandomTetrimino () is Tetrimino);
		}
		
		[Test]
		public void GetRandomTetriminoReturnsDifferentTetriminosTest ()
		{
			Tetrimino TetriminoA;
			Tetrimino TetriminoB;
			var maxLoops = 1000000;
			var loopCounter = 0;
			do {
				TetriminoA = _pf.GetRandomTetrimino ();
				TetriminoB = _pf.GetRandomTetrimino ();
				if (++loopCounter >= maxLoops) {
					throw new Exception (string.Format ("Loop reached {0} rounds, check implementation.", maxLoops));
				}
			} while (TetriminoA.CompareTo(TetriminoB) == 0);
		}
		
		[Test]
		public void GetRandomTetriminoReturnsSameTetriminosTest ()
		{
			Tetrimino TetriminoA;
			Tetrimino TetriminoB;
			var maxLoops = 1000000;
			var loopCounter = 0;
			do {
				TetriminoA = _pf.GetRandomTetrimino ();
				TetriminoB = _pf.GetRandomTetrimino ();
				if (++loopCounter >= maxLoops) {
					throw new Exception (string.Format ("Loop reached {0} rounds, check implementation.", maxLoops));
				}
			} while (TetriminoA.CompareTo(TetriminoB) != 0);
		}
	}
}

