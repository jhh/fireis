//  
//  TetriminioTest.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using NUnit.Framework;
using FireIsLib.Providers;

namespace FireIsLibTest.Providers
{
	public class TetriminioTest
	{
		private Tetrimino _tZ;
		private Tetrimino _tI;
		private Tetrimino _tS;
		
		[SetUp]
		public void Initialize() {
			_tZ = new Tetrimino{Name="Z", Shape0=new bool[2, 3]{{true,true,false},{false,true,true}}};
			_tI = new Tetrimino{Name="I", Shape0=new bool[4, 1]{{true},{true},{true},{true}}};
			_tS = new Tetrimino{Name="S", Shape0=new bool[2, 3]{{false, true, true},{true,true,false}}};
		}
		
		[Test]
		public void CompareToHandleSameTetriminosShapeTest(){
			var other = new Tetrimino{Name="Z", Shape0=(bool[,])_tZ.GetShape().Clone()};
			var expected = 0;
			Assert.AreEqual(_tZ.CompareTo(other), expected);
		}
		
		[Test]
		public void TetriminioShapeWithSmalerDimentionsReturns1Test() {
			var expected = 1;
			var actual = _tZ.CompareTo(_tI);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void TetriminioShapeWithLargerDimentionsReturnsMinus1Test() {
			var expected = -1;
			var actual = _tI.CompareTo(_tZ);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void ThisShapeWeightMoreWithCompareToThanMirroredTetriminoShapeTest() {
			var expected = 1;
			var actual = _tZ.CompareTo(_tS);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void ThisShapeWeightLessWithCompareToThanMirroredTetriminoShapeTest() {
			var expected = -1;
			var actual = _tS.CompareTo(_tZ);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void ComparingTwoEmptyTetriminosReturn0test() {
			var thisOne = new Tetrimino();
			var theOtherOne = new Tetrimino();
			var expeced = 0;
			Assert.AreEqual(expeced, thisOne.CompareTo(theOtherOne));
		}
		
		[Test]
		public void ComparingWithEmptyTetriminosDoNotReturn0test() {
			var emptyTetrimino = new Tetrimino();
			var expecedTestOne = 1;
			Assert.AreEqual(expecedTestOne, _tZ.CompareTo(emptyTetrimino));
			var expecedTestTwo = -1;
			Assert.AreEqual(expecedTestTwo, emptyTetrimino.CompareTo(_tZ));
		}
		
		[Test]
		public void EmptyTetriminoIsReallyEmpty() {
			Assert.Null(new Tetrimino().GetShape());
		}
		
		[Test]
		public void TurnTetrimino360DegreeRight() {
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeRight(), Tetrimino.Shapes.Shape90);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeRight(), Tetrimino.Shapes.Shape180);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeRight(), Tetrimino.Shapes.Shape270);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeRight(), Tetrimino.Shapes.Shape0);
		}
		
		[Test]
		public void TurnTetrimino360DegreeLeft() {
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeLeft(), Tetrimino.Shapes.Shape270);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeLeft(), Tetrimino.Shapes.Shape180);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeLeft(), Tetrimino.Shapes.Shape90);
			Assert.AreEqual(_tZ.RotateTetrimino90DegreeLeft(), Tetrimino.Shapes.Shape0);
		}
		
	}
}

