//  
//  TetriminoProviderTest.cs
//  
//  Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
//  Copyright (c) 2013 Jan Henrik Hasselberg
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using NUnit.Framework;
using FireIsLib.Providers;

namespace FireIsLibTest
{
	public class TetriminoProviderTest
	{
		private TetriminoProvider _tp;
			
		[SetUp]
		public void Initialize ()
		{
			_tp = new TetriminoProvider ();
		}
		
		[Test]
		public void ProviderHaveSevenTetriminoVariantsTest ()
		{
			int expects = 7;
			int countNames = _tp.GetTetriminoVariants ().Length;
			Assert.AreEqual (expects, countNames, string.Format ("PlayinField have {0} Tetrimino variants.", expects));
		}
		
		[Test]
		public void GetTetriminoByNameTest ()
		{
			var expected = new Tetrimino{Name="I",Shape0=new bool[4, 3]{{false, true, false},{false, true, false},{false, true, false},{false, true, false}}};
			var actual = _tp.GetTetriminoByName (expected.Name);
			Assert.IsTrue (expected.CompareTo (actual) == 0, "Expected Tetrimino is equal to the real Tetrimino.");
		}
		
		[Test]
		public void AllTetriminosHaveShapesForAllDegreesTest ()
		{
			var tetriminosHaveShapesForAllDegrees = true;
			foreach (var item in _tp.GetTetriminoVariants()) {
				if (
					item.GetShape () == null ||
					item.GetShape ().GetLength (0) == 0 ||
					item.GetShape (Tetrimino.Shapes.Shape90) == null ||
					item.GetShape (Tetrimino.Shapes.Shape90).GetLength (0) == 0 ||
					item.GetShape (Tetrimino.Shapes.Shape180) == null ||
					item.GetShape (Tetrimino.Shapes.Shape180).GetLength (0) == 0 ||
					item.GetShape (Tetrimino.Shapes.Shape270) == null ||
					item.GetShape (Tetrimino.Shapes.Shape270).GetLength (0) == 0
					) {
					tetriminosHaveShapesForAllDegrees = false;
					break;
				}
			}
			Assert.IsTrue (tetriminosHaveShapesForAllDegrees);
		}
		
		[Test]
		public void ResetMethodCreateNewVariantObject ()
		{
			var originalVariants = _tp.GetTetriminoVariants ();
			_tp.Reset ();
			var newVariants = _tp.GetTetriminoVariants ();
			Assert.AreNotSame (originalVariants, newVariants);
		}
		
		[Test]
		public void GetTetriminoVariantsReturnsSameVariantObject() {
			var originalVariants = _tp.GetTetriminoVariants ();
			var newVariants = _tp.GetTetriminoVariants ();
			Assert.AreSame (originalVariants, newVariants);
		}
	}
}

